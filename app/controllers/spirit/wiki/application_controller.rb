module Spirit::Wiki
  class ApplicationController < ::ApplicationController
    helper ApplicationHelper

    def current_ability
      @current_ability ||= Spirit::Wiki::Ability.new(current_user)
    end
  end
end
