$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "spirit/wiki/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "spirit-wiki"
  s.version     = Spirit::Wiki::VERSION
  s.authors     = ["powermedia"]
  s.email       = ["powermedia@qq.com"]
  s.homepage    = "http://sucj.net"
  s.summary     = Spirit::Wiki::DESCRIPTION
  s.description = Spirit::Wiki::DESCRIPTION
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5"
end

